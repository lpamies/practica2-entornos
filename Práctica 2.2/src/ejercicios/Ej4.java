package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
//El n�mero introducido no se puede dividir entre 0 por eso da error pero si lo cambiamos por un 1 en la i da bien
		for(int i = numeroLeido; i >= 1 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}
