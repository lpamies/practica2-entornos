package consolaJava;

import java.util.Scanner;

public class ConsolaJava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int opcion;
		Scanner input = new Scanner(System.in);
		
		System.out.println("1 - Opcion 1");
		System.out.println("2 - Opcion 2");
		System.out.println("3 - Opcion 3");
		System.out.println("4 - Opcion 4");
		System.out.println("Introduce un numero de opcion");
		
		opcion = input.nextInt();
		
		input.nextLine();
	
		switch (opcion) {
		
		case 1:
		
			int[] arrayEnteros = new int[10];
			
			for (int i = 0; i < arrayEnteros.length; i++) {
				
				System.out.println("Introduce un numero");
				arrayEnteros[i] = input.nextInt();
			}
			
			mostrarEstadisticas(arrayEnteros);
		
		break;
		
		case 2: 
			
			System.out.println("Introduce una palabra");
			
			String palabra = input.nextLine().toLowerCase();
			
			System.out.println(cifrar(palabra,3));	
			
		break;
		
		case 3:
			
			System.out.println(leeCadenas());
				
		break;	
			
		case 4:
			
			System.out.println("Introduce una cadena");
			String cadena = input.nextLine();
			
			System.out.println(invertirCadena(cadena));
		
		}
	
		input.close();
	
		}
	
		
	static void mostrarEstadisticas(int[] numero) {
			
		int mayor = 0;
		int menor = 0;
			
		int sumaPositivos = 0;
		int contadorPositivos = 0;
			
		int sumaNegativos = 0;
		int contadorNegativos = 0;
			
		int sumaTotal = 0;
			
		for (int i = 0; i < numero.length; i++) {
				
			sumaTotal = sumaTotal + numero[i];
				
			//Media negativos y suma
			if (numero[i] < 0) {
				contadorNegativos++;
				sumaNegativos = sumaNegativos + numero[i];
			}
				
			//Media positivos y suma
			if (numero[i] > 0) {
				contadorPositivos++;
				sumaPositivos = sumaPositivos + numero[i];
			}
				
			if (i == 0) {
				mayor = numero[i];
				menor = numero[i];
			}
				
			if (numero[i] > mayor) {
				mayor = numero[i];
			}
				
			if (numero[i] < menor) {
				menor = numero[i];
			}
		}
			
		System.out.println("Numero mayor: " + mayor);
		System.out.println("Numero menor: " + menor);
		System.out.println("Media positivos: " + (sumaPositivos/contadorPositivos));
		if(sumaNegativos < 0) {
			System.out.println("Media negativos: " + (sumaNegativos/(double)contadorNegativos));
		}
			System.out.println("Media total: " + (sumaTotal/numero.length));	
	}
	
	static String cifrar(String cadena, int desfase){
		
		String resultado = "";
		char caracter;
		int posicionCaracter;
		int desfaseDeZ;
		
		for(int i = 0; i < cadena.length(); i++){
			
			caracter = cadena.charAt(i);
			
			posicionCaracter = (int)caracter;
			posicionCaracter = posicionCaracter + desfase;
			
			caracter = (char)posicionCaracter;
			
			if(caracter > 'z') {
				desfaseDeZ = caracter - 'z';
				posicionCaracter = 'a' + desfaseDeZ - 1;
				caracter = (char)posicionCaracter;
			}
			
			resultado = resultado + caracter;
		}
		
		return resultado;
	}
	
	static String leeCadenas() {
	
		Scanner input = new Scanner(System.in);
	
		String cadena;
		String resultado = "";
	
		do {	
			System.out.println("Introduce una cadena");
			cadena = input.nextLine();	
		
			if (!cadena.equals("fin")) {
		
				resultado = resultado + cadena.substring(0, cadena.length()) + ";";
			}
		
		}while(!cadena.equals("fin"));

		input.close();

	return resultado;
	
	}
	
	static String invertirCadena(String cadena) {
		String cadenaInversa = "";
		
		for (int i = 0; i < cadena.length(); i++) {
			
			cadenaInversa = cadena.charAt(i) + cadenaInversa;
		}
		
	return cadenaInversa;
	
	}
}