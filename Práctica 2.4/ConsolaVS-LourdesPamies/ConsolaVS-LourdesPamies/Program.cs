﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVS_LourdesPamies
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Selecciona una opcion");
            Console.WriteLine("1 - Edad");
            Console.WriteLine("2 - Numeros Primos");
            Console.WriteLine("3 - Aleatorio");
            Console.WriteLine("4 - Lectura de Caracteres");

            String opcion = Console.ReadLine();

            switch (opcion)
            {
                case "1":

                    System.Console.WriteLine("Introduce una edad");
                    int numero = int.Parse(Console.ReadLine());

                    Console.WriteLine(Edad(numero));

                    break;

                case "2":

                    System.Console.WriteLine("Introduce un numero para activar el metodo");
                    int numeroLeido = int.Parse(Console.ReadLine());

                    Console.WriteLine(LeeEntero());

                    bool resultado;

                    for (int i = 0; i <= numeroLeido; i++)
                    {
                        resultado = EsPrimo(i);
                        if (resultado)
                        {
                            Console.WriteLine(i);
                        }
                    }

                    break;

                case "3":

                    System.Console.WriteLine("Genera un numero entre 0 y 100");

                    Console.WriteLine(aleatorio(100));

                    break;

                case "4":

                    Console.WriteLine(LeeCaracteres());

                    break;
            }

            Console.WriteLine("Pulsa una tecla para acabar");
            Console.ReadKey();
        }
        static bool Edad(int numero)
        {

            bool mayor = false;

            if (numero > 17)
            {
                mayor = true;
            }

            return mayor;
        }

        static bool EsPrimo(int numero)
        {


            int contadorDivisores = 0;

            for (int i = 1; i < numero; i++)
            {

                if (numero % i == 0)
                {

                    contadorDivisores++;
                }
            }

            if (contadorDivisores == 1)
            {
                return true;

            }

            else
            {
                return false;
            }
        }

        static int LeeEntero()
        {
            System.Console.WriteLine("Introduce un numero");

            int numero1 = int.Parse(Console.ReadLine());

            return numero1;
        }
        static int aleatorio(int fin)
        {
            Random random = new Random();
            int randomNumber = random.Next(0, 100);

            return randomNumber;
        }

        static String LeeCaracteres()
        {

            String cadena = " ";
            String resultado = " ";
            char b = Convert.ToChar(cadena.Substring(0, cadena.Length));

            do
            {
                Console.WriteLine("Introduce una cifra o letra y un 0 para finalizar");
                cadena = Console.ReadLine();

                if (b != '0')
                {

                    resultado = resultado + b + " ";
                }

            } while (cadena != "0");

            return resultado;
        }
    }

}
