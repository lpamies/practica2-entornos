package usoLibreriasJava;

import java.util.Scanner;

import libreria.MetodosDeInt;
import libreria.MetodosString;

public class UsoLibreriasJava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner input = new Scanner(System.in);
		
		System.out.println("Metodos de Int");
		
		System.out.println(MetodosDeInt.sumar(16, 34));
		System.out.println(MetodosDeInt.restar(32, 11));
		System.out.println(MetodosDeInt.multiplicar(5, 34));
		System.out.println(MetodosDeInt.dividir(64, 8));
		System.out.println(MetodosDeInt.resto(25, 5));
		
		System.out.println("Metodos de String");
		
		String cadena = "cerilla";
		String cadena2 = "Hola pedro";
		String cadena3 = "Hoy el dia esta muy nublado";
		String cadena4 = "Aerodinamico";
		String cadena5 = "Agua clara";
		
		MetodosString.cadenaInversa(cadena);
		System.out.println();
		MetodosString.Contiene(cadena2, input);
		System.out.println();
		MetodosString.fecha(cadena3, input);
		System.out.println();
		MetodosString.mostrarCaracteres(cadena4);
		System.out.println();
		MetodosString.sonIguales(cadena5, cadena2, input);

	}

}
