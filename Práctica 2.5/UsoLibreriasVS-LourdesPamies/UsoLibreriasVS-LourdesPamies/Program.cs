﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsoLibreriasVS_LourdesPamies
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Metodos de Int");

            Console.WriteLine("Suma");
            Console.WriteLine(LibreriaVS_LourdesPamies.MetodosDeInt.sumar(2, 9));

            Console.WriteLine("Resta");
            Console.WriteLine(LibreriaVS_LourdesPamies.MetodosDeInt.restar(4, 12));

            Console.WriteLine("Multiplicacion");
            Console.WriteLine(LibreriaVS_LourdesPamies.MetodosDeInt.multiplicar(8, 4));

            Console.WriteLine("Divion");
            Console.WriteLine(LibreriaVS_LourdesPamies.MetodosDeInt.dividir(24, 3));

            Console.WriteLine("Resto");
            Console.WriteLine(LibreriaVS_LourdesPamies.MetodosDeInt.resto(50, 10));

            Console.WriteLine("Metodos de String");

            String cadena = "platino";
            String cadena2 = "azulejo";
            String cadena3 = "fecha";
            String cadena4 = "vulcano";
            String cadena5 = "pluton";

            Console.WriteLine("Mostrar caracteres");
            LibreriaVS_LourdesPamies.MetodosString.mostrarCaracteres(cadena);

            Console.WriteLine();

            Console.WriteLine("Cadenas iguales");
            LibreriaVS_LourdesPamies.MetodosString.sonIguales(cadena, cadena2);

            Console.WriteLine();

            Console.WriteLine("Cadena inversa");
            LibreriaVS_LourdesPamies.MetodosString.cadenaInversa(cadena4);

            Console.WriteLine();

            Console.WriteLine("Fecha");
            LibreriaVS_LourdesPamies.MetodosString.fecha(cadena3);

            Console.WriteLine();

            Console.WriteLine("Contiene?");
            LibreriaVS_LourdesPamies.MetodosString.Contiene(cadena5);

            Console.WriteLine("Pulsa una tecla para terminar");
            Console.ReadKey();
        }
    }
}
