package libreria;

public class MetodosDeInt {
	 public static int sumar(int a, int b) {
         return a + b;
     }

     public static int restar(int a, int b)
     {
         return a - b;
     }

     public static int multiplicar(int a, int b)
     {
         return a * b;
     }

     public static int dividir(int a, int b)
     {
         return a / b;
     }

     public static int resto(int a, int b)
     {
         return a % b;
     }
}


