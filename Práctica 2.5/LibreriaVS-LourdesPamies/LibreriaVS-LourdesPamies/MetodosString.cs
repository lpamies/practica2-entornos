﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_LourdesPamies
{
   public class MetodosString
    {
        public static void mostrarCaracteres(String cadena)
        {

            for (int i = 0; i < cadena.Length; i++)
            {
                Console.WriteLine(cadena[i]);
            }

        }

        public static void sonIguales(String cadena, String cadena2)
        {

            Console.WriteLine("Introduce una cadena");

            String texto = Console.ReadLine();

            Console.WriteLine("Introduce una segunda cadena");

            String texto2 = Console.ReadLine();

            Console.WriteLine(texto.Contains(texto2));

        }

        public static void cadenaInversa(String cadena)
        {

            for (int i = cadena.Length - 1; i >= 0; i--)
            {
                Console.Write(cadena[i]);
            }

        }

        public static void fecha(String cadena)
        {

            Console.WriteLine("Dia: ");
            String Dia = Console.ReadLine();

            Console.WriteLine("Mes: ");
            String Mes = Console.ReadLine();

            Console.WriteLine("Year: ");
            String Year = Console.ReadLine();

            Console.Write(Dia + "/" + Mes + "/" + Year);

        }

        public static void Contiene(String cadena)
        {

            Console.WriteLine("Escribe una frase ");

            String texto = Console.ReadLine();

            Console.WriteLine(texto.Contains("a") || texto.Contains("e") || texto.Contains("i") || texto.Contains("o") || texto.Contains("u") ? "Contiene vocales" : "No contiene vocales");


        }
    }
}
