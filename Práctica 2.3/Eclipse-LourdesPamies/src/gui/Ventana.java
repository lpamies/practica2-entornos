package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JSlider;

public class Ventana extends JFrame {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * @author lourdes pamies
	 * @since 05/03/18
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setTitle("Formulario de env\u00EDo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 345);
		getContentPane().setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 11, 46, 14);
		getContentPane().add(lblNombre);
		
		textField = new JTextField();
		textField.setBounds(85, 8, 86, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(10, 45, 46, 14);
		getContentPane().add(lblApellidos);
		
		textField_1 = new JTextField();
		textField_1.setBounds(85, 42, 86, 20);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setBounds(10, 81, 46, 14);
		getContentPane().add(lblCorreo);
		
		JLabel lblEdad = new JLabel("Edad");
		lblEdad.setBounds(10, 116, 46, 14);
		getContentPane().add(lblEdad);
		
		JLabel lblTipoDeCarnet = new JLabel("Tipo de carnet");
		lblTipoDeCarnet.setBounds(10, 152, 76, 14);
		getContentPane().add(lblTipoDeCarnet);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(10, 183, 46, 14);
		getContentPane().add(lblSexo);
		
		textField_2 = new JTextField();
		textField_2.setBounds(85, 78, 86, 20);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setBounds(62, 179, 53, 23);
		getContentPane().add(rdbtnMujer);
		
		JRadioButton rdbtnHonbre = new JRadioButton("Hombre");
		rdbtnHonbre.setBounds(135, 179, 66, 23);
		getContentPane().add(rdbtnHonbre);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(85, 113, 29, 20);
		getContentPane().add(spinner);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Carnet A1", "Carnet A2", "Carnet A", "Carnet B", "Permiso BTP", "Carnet C1", "Carnet C", "Carnet D", "Carnet D1", "Carnet E"}));
		comboBox.setBounds(85, 149, 86, 20);
		getContentPane().add(comboBox);
		
		JLabel lblEstadoCivil = new JLabel("Estado civil");
		lblEstadoCivil.setBounds(202, 11, 66, 14);
		getContentPane().add(lblEstadoCivil);
		
		JRadioButton rdbtnCasada = new JRadioButton("Casada");
		rdbtnCasada.setBounds(260, 7, 66, 23);
		getContentPane().add(rdbtnCasada);
		
		JRadioButton rdbtnDivorciada = new JRadioButton("Divorciada");
		rdbtnDivorciada.setBounds(342, 7, 86, 23);
		getContentPane().add(rdbtnDivorciada);
		
		JRadioButton rdbtnSoltera = new JRadioButton("Soltera");
		rdbtnSoltera.setBounds(259, 41, 66, 23);
		getContentPane().add(rdbtnSoltera);
		
		JRadioButton rdbtnViuda = new JRadioButton("Viuda");
		rdbtnViuda.setBounds(344, 41, 66, 23);
		getContentPane().add(rdbtnViuda);
		
		JLabel lblOcupa = new JLabel("Ocupaci\u00F3n");
		lblOcupa.setBounds(202, 81, 66, 14);
		getContentPane().add(lblOcupa);
		
		JCheckBox chckbxTrabajando = new JCheckBox("Trabajando ");
		chckbxTrabajando.setBounds(258, 77, 97, 23);
		getContentPane().add(chckbxTrabajando);
		
		JCheckBox chckbxEstudiando = new JCheckBox("Estudiando");
		chckbxEstudiando.setBounds(258, 112, 97, 23);
		getContentPane().add(chckbxEstudiando);
		
		JCheckBox chckbxNada = new JCheckBox("Nada");
		chckbxNada.setBounds(352, 77, 76, 23);
		getContentPane().add(chckbxNada);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(323, 251, 89, 23);
		getContentPane().add(btnEnviar);
		
		JLabel lblFamilia = new JLabel("Familia");
		lblFamilia.setBounds(10, 216, 46, 14);
		getContentPane().add(lblFamilia);
		
		JSlider slider = new JSlider();
		slider.setMaximum(10);
		slider.setBounds(50, 208, 200, 26);
		getContentPane().add(slider);
		
		JLabel label = new JLabel("0    1    2    3    4    5    6    7    8    9   10");
		label.setBounds(53, 241, 215, 14);
		getContentPane().add(label);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("");
		menuBar.add(menu);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como...");
		mnArchivo.add(mntmGuardarComo);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnArchivo.add(mntmAbrir);
		
		JMenuItem mntmAbrirCon = new JMenuItem("Abrir con...");
		mnArchivo.add(mntmAbrirCon);
		
		JMenu mnInsertar = new JMenu("Insertar");
		menuBar.add(mnInsertar);
		
		JMenuItem mntmTabla = new JMenuItem("Tabla");
		mnInsertar.add(mntmTabla);
		
		JMenuItem mntmGrficos = new JMenuItem("Gr\u00E1ficos");
		mnInsertar.add(mntmGrficos);
		
		JMenuItem mntmFormas = new JMenuItem("Formas");
		mnInsertar.add(mntmFormas);
		
		JMenuItem mntmImgenes = new JMenuItem("Im\u00E1genes");
		mnInsertar.add(mntmImgenes);
		
		JMenu mnDiseo = new JMenu("Dise\u00F1o");
		menuBar.add(mnDiseo);
		
		JMenuItem mntmTemas = new JMenuItem("Temas");
		mnDiseo.add(mntmTemas);
		
		JMenuItem mntmColores = new JMenuItem("Colores");
		mnDiseo.add(mntmColores);
		
		JMenuItem mntmFuentes = new JMenuItem("Fuentes");
		mnDiseo.add(mntmFuentes);
		
		JMenuItem mntmEfectos = new JMenuItem("Efectos");
		mnDiseo.add(mntmEfectos);
		
		JMenu mnFormato = new JMenu("Formato");
		menuBar.add(mnFormato);
		
		JMenuItem mntmMrgenes = new JMenuItem("M\u00E1rgenes");
		mnFormato.add(mntmMrgenes);
		
		JMenuItem mntmOrientacin = new JMenuItem("Orientaci\u00F3n");
		mnFormato.add(mntmOrientacin);
		
		JMenuItem mntmSaltos = new JMenuItem("Saltos");
		mnFormato.add(mntmSaltos);
		
		JMenuItem mntmNmerosDeLnea = new JMenuItem("N\u00FAmeros de l\u00EDnea");
		mnFormato.add(mntmNmerosDeLnea);
	}
}
